<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Brand;

class BrandController extends Controller
{
    /**
    * @OA\Get(
    * path="/api/v1/brands",
    * operationId="getBrands",
    * tags={"Brand"},
    * summary="Get Brands List",
    * description="Get Brands Listr",
    *      @OA\Response(
    *          response=200,
    *          description="Successful",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function index(Request $request){
        $brands = Brand::paginate($request->perPage ?? 10);
        return response()->json($brands);
    }

    /**
    * @OA\Post(
    * path="/api/v1/brand/create",
    * operationId="createBrand",
    * tags={"Brand"},
    * summary="Create Brand",
    * description="Create Brand",
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"title","slug"},
    *               @OA\Property(property="title", type="text"),
    *               @OA\Property(property="slug", type="text")
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=201,
    *          description="Brand Created Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => 'required'
        ]);

        if ($validator->fails()) {
            $errorString = implode(" ", $validator->messages()->all());
            return response()->json(['error' => $errorString], 400);
        }

        $data = $request->only(['title', 'slug']);

        $brand = Brand::create($data);

        return response()->json([
            'brand' => $brand,
            'message' => "Brand created Successfully",
        ], 201);
    }

    /**
    * @OA\Put(
    * path="/api/v1/brand/edit/{uuid}",
    * operationId="Update Brand",
    * tags={"Brand"},
    * summary="Update Brand",
    * description="Update Brand",
    *   @OA\Parameter(
    *      name="uuid",
    *      in="query",
    *      required=true,
    *      @OA\Schema(
    *           type="string"
    *      )
    *   ),
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               @OA\Property(property="title", type="text"),
    *               @OA\Property(property="slug", type="text"),
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=200,
    *          description="Brand Updated Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function update(Request $request, $uuid){
        $brand = Brand::where('uuid', $uuid)->first();
        if(!$brand){
            return response()->json(["message" => "Brand not found"], 404);
        }
        
        $brand->update($request->only(['title', 'slug']));

        return response()->json([
            "brand" => $brand,
            "message" => "Brand updated successfully"
        ], 200);
    }

    /**
    * @OA\Delete(
    * path="/api/v1/brand/{uuid}",
    * operationId="Delete Brand",
    * tags={"Brand"},
    * summary="Delete Brand",
    * description="Delete Brand",
    *   @OA\Parameter(
    *      name="uuid",
    *      in="query",
    *      required=true,
    *      @OA\Schema(
    *           type="string"
    *      )
    *   ),
    *      @OA\Response(
    *          response=200,
    *          description="Brand deleted Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function destroy(Request $request, $uuid){
        $brand = Brand::where('uuid', $uuid)->first();
        if(!$brand){
            return response()->json(["message" => "Brand not found"], 404);
        }
      
        $brand->delete();

        return response()->json([
            "message" => "Brand deleted successfully"
        ], 200);
    }

    /**
    * @OA\Get(
    * path="/api/v1/brand/{uuid}",
    * operationId="Get Brand",
    * tags={"Brand"},
    * summary="Get Brand",
    * description="Get Brand",
    *   @OA\Parameter(
    *      name="uuid",
    *      in="query",
    *      required=true,
    *      @OA\Schema(
    *           type="string"
    *      )
    *   ),
    *      @OA\Response(
    *          response=200,
    *          description="Brand Fetched Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function show(Request $request, $uuid){
        $brand = Brand::where('uuid', $uuid)->first();
        if(!$brand){
            return response()->json(["message" => "Brand not found"], 404);
        }

        return response()->json($brand);
    }
}
