<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
    * @OA\Get(
    * path="/api/v1/categories",
    * operationId="getCatories",
    * tags={"Category"},
    * summary="Get Categories List",
    * description="Get Categories Listr",
    *      @OA\Response(
    *          response=200,
    *          description="Successful",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function index(Request $request){
        $categories = Category::paginate($request->perPage ?? 10);
        return response()->json($categories);
    }

    /**
    * @OA\Post(
    * path="/api/v1/category/create",
    * operationId="createCategory",
    * tags={"Category"},
    * summary="Create Category",
    * description="Create Category",
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"title","slug"},
    *               @OA\Property(property="title", type="text"),
    *               @OA\Property(property="slug", type="text")
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=201,
    *          description="Category Created Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => 'required'
        ]);

        if ($validator->fails()) {
            $errorString = implode(" ", $validator->messages()->all());
            return response()->json(['error' => $errorString], 400);
        }

        $data = $request->only(['title', 'slug']);

        $category = Category::create($data);

        return response()->json([
            'category' => $category,
            'message' => "Category created Successfully",
        ], 201);
    }

    /**
    * @OA\Put(
    * path="/api/v1/category/edit/{uuid}",
    * operationId="Category Brand",
    * tags={"Category"},
    * summary="Update Category",
    * description="Update Category",
    *   @OA\Parameter(
    *      name="uuid",
    *      in="query",
    *      required=true,
    *      @OA\Schema(
    *           type="string"
    *      )
    *   ),
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               @OA\Property(property="title", type="text"),
    *               @OA\Property(property="slug", type="text"),
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=200,
    *          description="Category Updated Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function update(Request $request, $uuid){
        $category = Category::where('uuid', $uuid)->first();
        if(!$category){
            return response()->json(["message" => "Category not found"], 404);
        }
        
        $category->update($request->only(['title', 'slug']));

        return response()->json([
            "category" => $category,
            "message" => "Category updated successfully"
        ], 200);
    }

    /**
    * @OA\Delete(
    * path="/api/v1/category/{uuid}",
    * operationId="Delete Category",
    * tags={"Category"},
    * summary="Delete Category",
    * description="Delete Category",
    *   @OA\Parameter(
    *      name="uuid",
    *      in="query",
    *      required=true,
    *      @OA\Schema(
    *           type="string"
    *      )
    *   ),
    *      @OA\Response(
    *          response=200,
    *          description="Category deleted Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function destroy(Request $request, $uuid){
        $category = Category::where('uuid', $uuid)->first();
        if(!$category){
            return response()->json(["message" => "Category not found"], 404);
        }
      
        $category->delete();

        return response()->json([
            "message" => "Category deleted successfully"
        ], 200);
    }

    /**
    * @OA\Get(
    * path="/api/v1/category/{uuid}",
    * operationId="Get Category",
    * tags={"Category"},
    * summary="Get Category",
    * description="Get Category",
    *   @OA\Parameter(
    *      name="uuid",
    *      in="query",
    *      required=true,
    *      @OA\Schema(
    *           type="string"
    *      )
    *   ),
    *      @OA\Response(
    *          response=200,
    *          description="Category Fetched Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function show(Request $request, $uuid){
        $category = Category::where('uuid', $uuid)->first();
        if(!$category){
            return response()->json(["message" => "Category not found"], 404);
        }

        return response()->json($category);
    }
}
