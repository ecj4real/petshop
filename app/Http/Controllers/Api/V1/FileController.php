<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\File;

class FileController extends Controller
{
    /**
    * @OA\Post(
    * path="/api/v1/file/upload",
    * operationId="uploadFile",
    * tags={"File"},
    * summary="Upload File",
    * description="Upload File",
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"file"},
    *               @OA\Property(property="file", type="file")
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=201,
    *          description="Category Created Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function upload(Request $request){
        $validator = Validator::make($request->all(), [
            'file' => 'required|file',
        ]);

        if ($validator->fails()) {
            $errorString = implode(" ", $validator->messages()->all());
            return response()->json(['error' => $errorString], 400);
        }

        $filePath = public_path() . '/files/';
        if (!\File::isDirectory($filePath)) {
            \File::makeDirectory($filePath);
        }

        $name = \Str::random(40);
        $file = $request->file;
        $size = $file->getSize();
        $ext = strtolower($file->getClientOriginalExtension());

        $file->move($filePath, $name.'.'.$ext);

        $db_file = File::create([
            'name' => $name,
            'path' => 'files/' . $name .'.'.$ext,
            'size' => $size,
            'type' => $ext
        ]);

        return response()->json(['file' => $db_file, 'message' => 'file uploaded Successfully']);
    }

    public function download(Request $request, $uuid){
        $file = File::where('uuid', $uuid)->first();
        if(!$file){
            return response()->json(["message" => "File not found"], 404);
        }

        $file_uri = public_path($file->path);
        return response()->download($file_uri);
    }
}
