<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Promotion;

class MainPageController extends Controller
{
    public function posts(Request $request){
        $post = Post::paginate($request->perPage ?? 10);
        return response()->json($post);
    }

    public function post(Request $request, $uuid){
        $post = Post::where('uuid', $uuid)->first();
        if($post){
            return response()->json($post);
        }
        return response()->json(["message" => "blog not found"], 404);
    }

    public function promotions(Request $request){
        $promotions = Promotion::paginate($request->perPage ?? 10);
        return response()->json($promotions);
    }
}
