<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\OrderStatus;

class OrderStatusController extends Controller
{
    public function index(Request $request){
        $order_statuses = OrderStatus::paginate($request->perPage ?? 10);
        return response()->json($order_statuses);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            $errorString = implode(" ", $validator->messages()->all());
            return response()->json(['error' => $errorString], 400);
        }

        $data = $request->only(['title', 'slug']);

        $order_status = OrderStatus::create($data);

        return response()->json([
            'order_status' => $order_status,
            'message' => "OrderStatus created Successfully",
        ], 201);
    }

    public function update(Request $request, $uuid){
        $order_status = OrderStatus::where('uuid', $uuid)->first();
        if(!$order_status){
            return response()->json(["message" => "OrderStatus not found"], 404);
        }
        
        $order_status->update($request->only(['title', 'slug']));

        return response()->json([
            "order_status" => $order_status,
            "message" => "OrderStatus updated successfully"
        ], 200);
    }

    public function destroy(Request $request, $uuid){
        $order_status = OrderStatus::where('uuid', $uuid)->first();
        if(!$order_status){
            return response()->json(["message" => "OrderStatus not found"], 404);
        }
      
        $order_status->delete();

        return response()->json([
            "message" => "OrderStatus deleted successfully"
        ], 200);
    }

    public function show(Request $request, $uuid){
        $order_status = OrderStatus::where('uuid', $uuid)->first();
        if(!$order_status){
            return response()->json(["message" => "OrderStatus not found"], 404);
        }

        return response()->json($order_status);
    }
}
