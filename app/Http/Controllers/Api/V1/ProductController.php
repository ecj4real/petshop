<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;

class ProductController extends Controller
{
    public function index(Request $request){
        $products = Product::paginate($request->perPage ?? 10);
        return response()->json($products);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'category_uuid' => 'required|exists:categories,uuid',
            'title' => 'required',
            'price' => 'required',
            'description' => 'required',
            'brand_uuid' => 'required|exists:brands,uuid',
            'image_uuid' => 'required|exists:files,uuid'
        ]);

        if ($validator->fails()) {
            $errorString = implode(" ", $validator->messages()->all());
            return response()->json(['error' => $errorString], 400);
        }

        $data = $request->only(['category_uuid', 'title', 'price', 'description']);
        $data['metadata'] = [
            "brand" => $request->brand_uuid,
            "image" => $request->image_uuid
        ];

        $product = Product::create($data);

        return response()->json([
            'product' => $product,
            'message' => "Product created Successfully",
        ], 201);
    }

    public function update(Request $request, $uuid){
        $validator = Validator::make($request->all(), [
            'category_uuid' => 'exists:categories,uuid',
            'brand_uuid' => 'exists:brands,uuid',
            'image_uuid' => 'exists:files,uuid'
        ]);

        if ($validator->fails()) {
            $errorString = implode(" ", $validator->messages()->all());
            return response()->json(['error' => $errorString], 400);
        }

        $product = Product::where('uuid', $uuid)->first();
        if(!$product){
            return response()->json(["message" => "Product not found"], 404);
        }
        
        $product->update($request->only(['category_uuid', 'title', 'price', 'description']));
        if($request->brand_uuid){
            $product->update(['metadata->brand' => $request->brand_uuid]);
        }
        if($request->image_uuid){
            $product->update(['options->image' => $request->image_uuid]);
        }

        return response()->json([
            "product" => $product,
            "message" => "Product updated successfully"
        ], 200);
    }

    public function destroy(Request $request, $uuid){
        $product = Product::where('uuid', $uuid)->first();
        if(!$product){
            return response()->json(["message" => "Product not found"], 404);
        }
      
        $product->delete();

        return response()->json([
            "message" => "Product deleted successfully"
        ], 200);
    }

    public function show(Request $request, $uuid){
        $product = Product::where('uuid', $uuid)->first();
        if(!$product){
            return response()->json(["message" => "Product not found"], 404);
        }

        return response()->json($product);
    }
}
