<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Firebase\JWT\JWT;
use App\Models\User;
use App\Models\JwtToken;
use Auth;

class UserController extends Controller
{
    /**
    * @OA\Post(
    * path="/api/v1/user/create",
    * operationId="Register",
    * tags={"User"},
    * summary="Register User",
    * description="User Register here",
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"first_name","last_name", "email", "address", "phone_number", "password"},
    *               @OA\Property(property="first_name", type="text"),
    *               @OA\Property(property="last_name", type="text"),
    *               @OA\Property(property="email", type="text"),
    *               @OA\Property(property="address", type="text"),
    *               @OA\Property(property="phone_number", type="text"),
    *               @OA\Property(property="password", type="password")
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=201,
    *          description="Register Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    * )
    */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:25',
            'last_name' => 'required|max:25',
            'email' => 'required|email|unique:users,email',
            'address' => 'required',
            'phone_number' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $errorString = implode(" ", $validator->messages()->all());
            return response()->json(['error' => $errorString], 400);
        }

        $data = $request->only(['first_name', 'last_name', 'email', 'address', 'phone_number', 'password']);
        $data['password'] = bcrypt($data['password']);
        $data['last_login_at'] = now();

        $user = User::create($data);

        $token = JWT::encode(['user_uuid'=>$user->uuid], config('jwt.key'), 'HS256');

        //Save token to DB
        JwtToken::create(['user_id' => $user->id, 'unique_id' => $token, 'token_title' => config('jwt.token_title')]);

        return response()->json([
            'user' => $user,
            'token' => $token,
        ], 201);
    }

    /**
    * @OA\Post(
    * path="/api/v1/user/login",
    * operationId="authLogin",
    * tags={"User"},
    * summary="User Login",
    * description="Login User Here",
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"email", "password"},
    *               @OA\Property(property="email", type="email"),
    *               @OA\Property(property="password", type="password")
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=200,
    *          description="Login Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    * )
    */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $errorString = implode(" ", $validator->errors()->all());
            return response()->json(['error' => $errorString], 400);
        }
        $status = 400;
        $response = ['error' => 'Unauthorised'];
        if (Auth::attempt($request->only(['email', 'password']))) {
            $status = 200;
            $user = Auth::user();
            $token = JWT::encode(['user_uuid'=>$user->uuid], config('jwt.key'), 'HS256');

            //Save token to DB
            $jwt_token = JwtToken::where(['user_id' => $user->id, 'unique_id' => $token])->first();
            if(!$jwt_token){
                JwtToken::create(['user_id' => $user->id, 'unique_id' => $token, 'token_title' => config('jwt.token_title')]);
            }

            $user->last_login_at = now();
            $user->save();

            $response = [
                'user' => $user,
                'token' => $token,
            ];
        }
        return response()->json($response, $status);
    }

    /**
    * @OA\Get(
    * path="/api/v1/user/logout",
    * operationId="authLogout",
    * tags={"User"},
    * summary="User logout",
    * description="Login User Here",
    *      @OA\Response(
    *          response=200,
    *          description="Logout Successfull",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *       security={{ "apiAuth": {} }}
    * )
    */
    public function logout() {
        $user = Auth::user();
        JwtToken::where('user_id', $user->id)->delete();
        
        return response()->json([
            'status' => 'success',
            'message' => 'logout'
        ], 200);
    }


    /**
    * @OA\Get(
    * path="/api/v1/user",
    * operationId="getUser",
    * tags={"User"},
    * summary="Get aUser",
    * description="Returns authenticated User",
    *      @OA\Response(
    *          response=200,
    *          description="Successful",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function getUserViaToken()
    {
        $user = Auth::user();
        return response()->json($user);
    }

    /**
    * @OA\Put(
    * path="/api/v1/user/edit",
    * operationId="Update User",
    * tags={"User"},
    * summary="Update User",
    * description="Update user",
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               @OA\Property(property="first_name", type="text"),
    *               @OA\Property(property="last_name", type="text"),
    *               @OA\Property(property="email", type="text"),
    *               @OA\Property(property="address", type="text"),
    *               @OA\Property(property="phone_number", type="text"),
    *               @OA\Property(property="password", type="password")
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=200,
    *          description="User Updated Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *      security={{ "apiAuth": {} }}
    * )
    */
    public function updateUser(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name' => 'max:25',
            'last_name' => 'max:25',
            'email' => 'email|unique:users,email'
        ]);

        $user = Auth::user();
        $user->update($request->only(['first_name', 'last_name', 'address', 'phone_number']));

        return response()->json([
            "user" => $user,
            "message" => "User updated successfully"
        ], 200);
    }

    /**
    * @OA\Delete(
    * path="/api/v1/user",
    * operationId="deleteUser",
    * tags={"User"},
    * summary="Delete User",
    * description="Deletes authenticated User",
    *      @OA\Response(
    *          response=200,
    *          description="Successful",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *       security={{ "apiAuth": {} }}
    * )
    */
    public function deleteUser(Request $request){
        $user = Auth::user();
        $user->delete();

        return response()->json([
            "message" => "User deleted successfully"
        ], 200);
    }
}
