<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidModel;

class Category extends Model
{
    use HasFactory, UuidModel;

    protected $fillable = [
        'title', 'slug'
    ];

    protected $table = 'categories';
}
