<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UuidModel;

class Product extends Model
{
    use HasFactory, UuidModel, SoftDeletes;

    protected $fillable = ['category_uuid', 'title', 'price', 'description', 'metadata'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'metadata' => 'array'
    ];
}
