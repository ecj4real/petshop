<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\JwtToken;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Auth::viaRequest('jwt', function (Request $request) {
            try{
                $decoded = JWT::decode($request->bearerToken(), new Key(config('jwt.key'), 'HS256'));
                $decoded_array = (array) $decoded;

                $user = User::where('uuid', $decoded_array['user_uuid'])->first();
                if($user){
                    $token = str_replace("Bearer ","",$request->bearerToken());
                    $jwt_token = JwtToken::where(['user_id' => $user->id, 'unique_id' => $token])->first();
                    if($jwt_token){
                        return $user;
                    }
                }
                return null;
            } catch(\Exception $th){
                Log::error($th);
                return null;
            }
        });
    }
}
