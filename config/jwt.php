<?php

return [
    'key' => env('JWT_KEY', 'secret'),
    'token_title' => 'petshop'
];