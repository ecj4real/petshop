<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class PromotionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->word(),
            'content' => fake()->text(),
            'metadata' => [
                "valid_from" => now()->format('Y-m-d'),
                "valid_to" => now()->format('Y-m-d'),
                "image" => \Str::uuid()
            ]
        ];
    }
}
