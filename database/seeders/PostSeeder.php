<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Post;
use DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('posts')->truncate();
        Post::factory()->count(10)->create();
        // $table->string('title');
        //     $table->string('slug');
        //     $table->text('content');
        //     $table->json('metadata');
    }
}
