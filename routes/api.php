<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1'], function () {
    Route::group(['prefix' => '/user'], function(){
        Route::post('create', 'UserController@register');
        Route::post('login', 'UserController@login');
        Route::group(['middleware' => ['auth:api']], function () {
            Route::get('logout', 'UserController@logout');
            Route::get('/', 'UserController@getUserViaToken');
            Route::put('edit', 'UserController@updateUser');
            Route::delete('/', 'UserController@deleteUser');
        });
    });

    Route::group(['prefix' => '/main'], function(){
        Route::get('blog', 'MainPageController@posts');
        Route::get('blog/{uuid}', 'MainPageController@post');
        Route::get('promotions', 'MainPageController@promotions');
    });

    Route::get('/brands', 'BrandController@index');
    Route::group(['prefix' => '/brand'], function(){
        Route::group(['middleware' => ['auth:api', 'admin']], function(){
            Route::post('create', 'BrandController@store');
            Route::put('/{uuid}', 'BrandController@update');
            Route::delete('/{uuid}', 'BrandController@destroy');
        });
        Route::get('/{uuid}', 'BrandController@show');
    });

    Route::get('/categories', 'CategoryController@index');
    Route::group(['prefix' => '/category'], function(){
        Route::group(['middleware' => ['auth:api', 'admin']], function(){
            Route::post('create', 'CategoryController@store');
            Route::put('/{uuid}', 'CategoryController@update');
            Route::delete('/{uuid}', 'CategoryController@destroy');
        });
        Route::get('/{uuid}', 'CategoryController@show');
    });
    
    Route::group(['prefix' => '/file'], function(){
        Route::post('upload', 'FileController@upload')->middleware(['auth:api', 'admin']);
        Route::get('/{uuid}', 'FileController@download');
    });

    Route::get('/order-statuses', 'OrderStatusController@index');
    Route::group(['prefix' => '/order-status'], function(){
        Route::group(['middleware' => ['auth:api', 'admin']], function(){
            Route::post('create', 'OrderStatusController@store');
            Route::put('/{uuid}', 'OrderStatusController@update');
            Route::delete('/{uuid}', 'OrderStatusController@destroy');
        });
        Route::get('/{uuid}', 'OrderStatusController@show');
    });

    Route::get('/products', 'ProductController@index');
    Route::group(['prefix' => '/product'], function(){
        Route::group(['middleware' => ['auth:api', 'admin']], function(){
            Route::post('create', 'ProductController@store');
            Route::put('/{uuid}', 'ProductController@update');
            Route::delete('/{uuid}', 'ProductController@destroy');
        });
        Route::get('/{uuid}', 'ProductController@show');
    });
});