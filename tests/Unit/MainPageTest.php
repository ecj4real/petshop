<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Post;
use App\Models\Promotion;

class MainPageTest extends TestCase
{
    use RefreshDatabase;

    public function testListAllPost(){
        $posts = Post::factory()->count(10)->create();
        
        $response = $this->get('/api/v1/main/blog');

        $response->assertStatus(200);
    }

    public function testListAllPromotions(){
        $posts = Promotion::factory()->count(10)->create();
        
        $response = $this->get('/api/v1/main/promotions');

        $response->assertStatus(200);
    }

    public function testGetAPost(){
        $post = Post::factory()->create();
        
        $response = $this->get('/api/v1/main/blog/'.$post->uuid);

        $response->assertStatus(200);
    }

    public function testPostDoesnotExist(){
        
        $response = $this->get('/api/v1/main/blog/'.'invalid-uuid');

        $response->assertStatus(404);
    }
}
