<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testCreatingUser(){
        $data = [
            'first_name' => 'Emeka',
            'last_name' => 'Okon',
            'email' => 'another@test.com',
            'address' => '14 Johson Street, Lagos Nigeria',
            'phone_number' => '2348090988909',
            'password' => 'password'
        ];

        $response = $this->post('/api/v1/user/create', $data);
        $response->assertStatus(201);
        $response->assertJsonStructure(['user', 'token']);
    }

    public function testUserCanLogin(){
        $user = User::factory()->create();
 
        $response = $this->post('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['user', 'token']);
    }

    public function testUserCantLogin(){
        $user = User::factory()->create();
 
        $response = $this->post('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'wrongPassword',
        ]);

        $response->assertStatus(400);
    }

    public function testUnauthenticatedUserCantDelete(){
        $user = User::factory()->create();
        
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX3V1aWQiOiJkMmQ2ZDE3Mi0wMzJiLTQ2NTEtODIzYS02NWY2Y2Y2NWYwMjEifQ.9Qwb6zvTUEEwcKpGKoHPsCDw65Pk2ZHPVaiFMxbiqW1",
            'Accept' => 'application/json'
        ])->delete('/api/v1/user');

        $response->assertStatus(401);
    }

    public function testAuthenticatedUserCanDelete(){
        $user = User::factory()->create();
        
        $response = $this->post('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['user', 'token']);

        $delete_response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $response->decodeResponseJson()['token'],
            'Accept' => 'application/json'
        ])->delete('/api/v1/user');

        $delete_response->assertStatus(200);
    }

}
